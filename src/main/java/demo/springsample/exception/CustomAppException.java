package demo.springsample.exception;

public class CustomAppException extends RuntimeException {

    public CustomAppException(Exception cause) {
        super(cause);
    }

}
