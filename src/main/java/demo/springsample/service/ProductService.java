package demo.springsample.service;

import demo.springsample.exception.CustomAppException;
import demo.springsample.model.Product;
import demo.springsample.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    @Cacheable("findAllProducts")
    public Page<Product> findAll(Pageable pageable) {
        try {
            return productRepository.findAll(pageable);
        } catch (Exception ex) {
            throw new CustomAppException(ex);
        }
    }

    @CacheEvict(value = "findAllProducts", allEntries = true)
    public Product save(Product product) {
        try {
            return productRepository.save(product);
        } catch (Exception ex) {
            throw new CustomAppException(ex);
        }
    }

    public Optional<Product> findById(Long id) {
        try {
            return productRepository.findById(id);
        } catch (Exception ex) {
            throw new CustomAppException(ex);
        }
    }

}
