package demo.springsample.controller;

import java.sql.Timestamp;
import java.util.concurrent.CompletableFuture;

import demo.springsample.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import demo.springsample.model.Product;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    private Product addNew(@RequestBody Product fruit) {
        return productService.save(fruit);
    }

    @PutMapping("/{id}")
    private Product update(@PathVariable Long id, @RequestBody Product fruit) {
        var existing = productService.findById(id).orElseThrow(() -> new RuntimeException("Record not found"));
        existing.setName(fruit.getName());
        existing.setColor(fruit.getColor());
        return productService.save(existing);
    }

    @GetMapping
    public Page<Product> getPaginatedResponse(@RequestParam int pageNumber, @RequestParam int pageSize) {
        return productService.findAll(PageRequest.of(pageNumber, pageSize));
    }
    @GetMapping("/health")
    public Timestamp serviceUnavailable() {
    	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(timestamp);
        return timestamp;
    }

    @Async
    @GetMapping("/export")
    public ResponseEntity<Void> exportProductsAsync() {
        CompletableFuture.runAsync(() -> {
            // all logic executed here will run in a separedted thread
            // the exported products can be uploaded to s3 or send to e-mail
        });
        return ResponseEntity.ok().build();
    }

}
